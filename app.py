import base64
import os
from datetime import datetime
from flask import Flask, request, make_response, g, jsonify
from flask_httpauth import HTTPTokenAuth
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from sqlalchemy.dialects.postgresql import UUID
import json
from uuid import UUID as UUID_ins
from decimal import Decimal

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://postgres:ZAQ!2wsx@localhost:5432/web_api"
app.config['SECRET_KEY'] = os.urandom(32)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
token_auth = HTTPTokenAuth()


class JsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID_ins):
            return str(obj)
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(), index=True, unique=True)
    token = db.Column(db.String(), index=True, unique=True)
    created = db.Column(db.DateTime)

    def get_token(self):
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        db.session.add(self)
        return self.token

    @staticmethod
    def check_token(token):
        return User.query.filter_by(token=token).first()


class Url(db.Model):
    __tablename__ = 'urls'

    id = db.Column(UUID(as_uuid=True), primary_key=True)
    path = db.Column(db.String())
    created = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    title = db.Column(db.String())


class Event(db.Model):
    __tablename__ = 'events'

    id = db.Column(db.Integer, primary_key=True)
    status_code = db.Column(db.Integer)
    url_id = db.Column(UUID(as_uuid=True), db.ForeignKey('urls.id'))
    response_time = db.Column(db.Integer)
    created = db.Column(db.DateTime)
    response_size = db.Column(db.Integer)
    active = db.Column(db.Boolean)  # Флаг активности события (используется в методе delete)

    def to_dict(self):
        data = {
            'url_id': self.url_id,  
            'created': self.created,
            'response_time': self.response_time,
            'status_code': self.status_code
        }

        return data


admin = Admin(app)
admin.add_view(ModelView(User, db.session))


@token_auth.verify_token
def verify_token(token):
    g.current_user = User.check_token(token) if token else None
    return g.current_user is not None


@token_auth.error_handler
def token_auth_error():
    return make_response({"error": "Auth is failed"}, 401)


@app.route('/add_urls', methods=['POST'])
@token_auth.login_required
def add_urls():
    if request.method == 'POST':
        if request.is_json:
            data = request.get_json()
            for row in data:
                new_url = Url(id=row.get('id'),
                              path=row.get('path'),
                              title=row.get('title'),
                              user_id=g.current_user.id,
                              created=datetime.now())
                db.session.add(new_url)
                db.session.commit()
            return {"message": f"Urls has been added successfully."}
        else:
            return {"error": "The request payload is not in JSON format"}


@app.route('/urls/<url_id>', methods=['DELETE'])
@token_auth.login_required
def delete_url(url_id):
    if request.method == 'DELETE':
        Event.query.filter_by(url_id=url_id).update({Event.active: False})
        db.session.commit()
        return {"message": f"Url {url_id} successfully deactivated."}


@app.route('/events/<url_id>', methods=['GET'])
@token_auth.login_required
def events(url_id):
    skip = request.args.get('skip', None)
    limit = request.args.get('limit', None)
    query = Event.query.filter_by(url_id=url_id)
    if limit:
        query = query.limit(limit)
    if skip:
        query = query.offset(skip)
    query = query.all()
    return jsonify([row.to_dict() for row in query])


@app.route('/statistic', methods=['GET'])
@token_auth.login_required
def statistic():
    req_response_time = request.args.get('response_time', None) 
    if not req_response_time:
        return make_response({"error": "The parameter 'response_time' is missed"}, 400)
    start, end = tuple(map(int, req_response_time.split(',')))
    # По заданию требовалась сортировка
    # sort=created|response_time|status_code|response_size
    # но в выходных столбцах невозможно сортировать по указанным столбцам,
    # поэтому сортировка сделана по реальным столбцам

    sort = request.args.get('sort', None)
    if not sort or sort not in ['url_id', 'avg_response_time', 'success_rate', 'max_response_size']:
        return make_response({"error": "The parameter 'sort' is missed or incorrect. "
                                       "Correct values are "
                                       "'url_id', 'avg_response_time', 'success_rate', 'max_response_size'"}, 400)

    req_status_code = request.args.get('status_code', None)
    if req_status_code:
        status_condition = f"and status_code={req_status_code}"
    else:
        status_condition = ""
    result = db.engine.execute(f"""
    select url_id,  
           round(avg(response_time),0) as avg_response_time, 
           round(sum(case when status_code between 200 and 399 then 1.0 else 0.0 end)/count(*),2) as success_rate,
           max(response_size) as max_response_size
    from events
    where response_time between {start} and {end} 
    {status_condition}          
    group by url_id
    order by {sort};
    """)

    return json.dumps([dict(row) for row in result], cls=JsonEncoder)


if __name__ == '__main__':
    app.run()
